# Configuration variables d'environnement

## Prérequis : **Copier le fichier .env.example vers .env.**

Configurer les variables d'environnement suivantes :

## Réglages basiques
### Le port sur lequel tourne le serveur

Doit être en adéquation avec le VirtualHost configuré.
```
PORT=3035
```

### L'adresse du serveur, nécessairement visible depuis internet, qui servira de proxy pour les images

Il est possible que les images soient visible via des liens internes à GRR, qui ne sont visible que sur l'intranet ; le serveur fait office de proxy pour visualiser ces images hors du réseau école.
```
SERVER_URL=https://domain.local/
```

### Adresse interne de GRR

Pour cette raison, il est nécessaire d'indiquer au serveur l'endroit où trouver les images sur GRR.
Si ce champ n'est pas renseigné, l'adresse externe `GRR_URL` sera utilisée.
```
GRR_URL_PROXY=https://grr.example-interne.com/
```
Note : cette adresse n'est censée être visible que sur le réseau interne de l'école.

### Adresse externe de GRR

Dans le cas de demande de modération pour l'emprunt de certaines ressources, un lien est envoyé à l'administrateur (spécifié dans `GRR_MODERATION_RESERVATION_MAIL`). Dans certaines configuration, ce lien doit pouvoir être accessible de l'extérieur de l'école : il faut donc une adresse externe visible depuis le réseau externe, pour que les modérateurs puissent intervenir à distance.

```
GRR_URL=https://grr.example.com/
```

### Type de réservation par défaut 
Pour les réservations rapides, parmi celles déclarées dans admin/admin_type.php
```
GRR_DEFAULT_RESERVATION_TYPE=A
```


### Fonctionnalité "ressource empruntée/restituée"
Est-ce qu'il faut relancer par email les utilisateurs ayant des ressources en retard, dans le cas des ressources dont cette case est cochée dans la configuration ?
Si au moins une des ressources sur GRR utilise cette fonctionnalité, vous devez mettre ce paramètre à "true". Sinon, mettre "false".

```
GRR_DEFAULT_RESERVATION_RETARD_EMAIL=true
```

### Adresse du endpoint de Prismic, pour les actualités et descriptions

```
PRISMIC_URL=https://subdomain.prismic.io/api/v2
```

### Adresse du module libre service de Koha

```
KOHA_URL=https://www.example.com/cgi-bin/koha/sco/sco-main.pl
```

Il est possible que le module libre service soit protégé par une identification. Dans ce cas, il est nécessaire d'avoir à disposition un compte 
partagé sans date d'expiration, qu'il faut renseigner via ces champs :

```
KOHA_AUTHWALL_LOGIN=identifiant
KOHA_AUTHWALL_PASSWORD=motdepasse
```

Il est nécessaire d'indiquer la version de Koha, car les messages d'erreurs diffèrent selon les versions.
Cet identifiant doit correspondre à un fichier JSON déjà présent dans `/api/koha/error_codes`. Si ce n'est pas le cas, vous pouvez copier un des fichiers, et adapter les Regex `error_code_match` aux codes d'erreurs de votre instance.

Vous pouvez rechercher la balise meta "generator" dans le code source de votre site pour connaitre ce numéro de version.

```
KOHA_VERSION=19.1112000
```

### Adesses des fichiers ICS (via zimbra par exemple)

```
AGENDA_ICS_URL=https://zimbra.example.com/home/user@domain.fr/Agenda.ics
HORAIRES_ICS_URLS=[{ "campus_id": 1, "campus_code": "FR", "url": 'https://zimbra.example.com/home/user@domain.fr/Agenda.ics'}]
```

Il est possible d'ajouter un sous-titre, affiché en dessous du titre dans l'écran des horaires, en ajoutant au sein d'un objet la propriété suivante dans `HORAIRES_ICS_URLS` :

```json
 "subname" : {"fr":"Exemple de sous-titre", "en": "Subtitle example"}
```

Il est nécessaire de traduire les sous-titre à ce niveau, car l'application ne contient pas les traductions à l'avance de ce champ optionnel.

### Le SMTP (sans authentification) nécessaire à l'envoi de mail

```
SMTP_HOST=smtp://smtp.example.fr
```

### Adresse mail utilisée comme expéditrice

```
SMTP_FROM=noreply@example.fr
```

### Les informations de connexion à la base de données GRR (les connexions doivent être autorisées depuis ce host)

L'utilisateur doit avoir accès aux tables grr_entry en lecture / écriture, et toutes les autres en lecture.

```bash
MYSQL_HOST=localhost
MYSQL_USER=root
MYSQL_PASSWORD=toor
MYSQL_PORT=3306
MYSQL_DATABASE=grr
MYSQL_WAITFORCONNECTIONS=true
MYSQL_CONNECTIONLIMIT=10
MYSQL_QUEUELIMIT=0
```

### Pour la récupération sélectives des éléments de GRR. 

Mettre `[]` pour tous, ou `[number, ...]`

```
IDS_AREA_MATERIEL=[]
IDS_AREA_SALLES=[]
```


### Adresses mails de contacts

Les numéros doivent correspondre à ceux rentrés dans l'application, pour l'internationalisation des champs via les fichiers de traduction.

```json
SUGGESTION_ALIAS_MAIL_CATEGORIES={"1": "services@example.com", "2" : "ressources@example.com", "3" : "apprentissage-enseignement@example.com", "4" : "outils-pedagogiques@example.com", "5" : "lieux-apprentissage@example.com", "6" : "application-intranet@example.com", "7" : "autres@example.com"}

RDV_ALIAS_MAIL_DESTINATAIRE={"1" : "conseiller-pedagogique@example.com", "2" : "ingenieur-pedagogique@example.com", "3" : "documentaliste@example.com", "4" : "conseiller-juridique@example.com", "5" : "technicien-video@example.com", "6" : "direction@example.com"}

DEMANDE_REFERENCE_ALIAS_MAIL=documentaliste@example.com
```


### Mail de support technique

Adresse e-mail affichée en cas d'erreur de connexion shibboleth, pour que l'utilisateur contacte le support en cas d'erreur.

```
SUPPORT_TECHNIQUE_MAIL=dsi@example.com
```

## Réglages supplémentaires

### Blacklist de nom de salles

Pour empêcher la récupération de salle, basé sur une recherche textuelle sur le nom de la salle. 
Par exemple, ["Cabane verte"]. Laisser vide si aucun usage (ne pas commenter)
BLACKLIST_SALLE_NAME=[]

### Meta données

Des meta-informations incrustées par défaut sur les pages générées par le serveur, utilisée par Prismic. Utile uniquement pour afficher des informations personnalisées lors du partage d'une page de LCA via les réseaux sociaux.

```
SEO_TITLE=LCA
SEO_DESCRIPTION=LCA
SEO_IMAGE=
```


### Préfixe / suffixe des code barres

Cas spécifique où un préfixe ou un suffixe doit être ajouté à tous les code barres.

```
KOHA_BARCODE_PREFIX=1
KOHA_BARCODE_SUFFIX=234
```

### Page KOHA avec une frame

Cas où la page de module libre service est intégrée dans une autre page en iframe. Renseigner le même nom de domaine que KOHA_URL, pour feindre que les appels viennent du cadre supérieur.

```
KOHA_REFERER_URL=https://www.example.com
```


### Ressources modérées 

Utilisé pour alerter des administrateurs, sur les salles et matériel nécessitant une modération, qu'une réservation a été effectuée

```
GRR_MODERATION_RESERVATION_MAIL=alias@example.com
```



### Whitelist des noms de domaines d'adresses e-mail qui peuvent accéder à l'appli (utilisé en cas d'usage de WAYF). 

Par exemple : ["mines-stetienne.fr", "imt.fr"]. Mettre à [] empechera tout utilisateur d'accéder à l'application.

```
SHIBBOLETH_DN_WHITELIST=[]
```

### API de récupération de numéro de carte

Récupération du numéro de carte via une API externe. Remplacera {uid} par la propriété uid de l'utilisateur connecté

```
SHIBBOLETH_RUCCARTE_API=https://api.example.com/uid/{uid}/
```


## Réglages de debug

### Niveau de log

Pour avoir plus d'informations sur les paquets entrants :

```
NODE_LOGGER_LEVEL=debug
```

### Utilisateur shibboleth de test

A utiliser tant que shibboleth n'est pas configuré (= pas de protection sur /api/shibboleth/login). Ne nécessite pas de modifier l'application.

```
SHIBBOLETH_TEST_USER={ "eppn": "user@dn.fr", "userId": "username", "email": "user.name@dn.fr", "name": "User Name", "id_koha": "000000000000", "formation": "ELEVE", "service": "ELEVES-NA", "organization": "Organisation" }
```