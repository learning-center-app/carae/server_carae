// Load environment variables from .env file
const dotenv = require('dotenv');
const _env_vars = dotenv.load();
if (_env_vars.error) {
  throw _env_vars.error;
}
//Vérification des variables d'environnementd
const missing_env_vars = ["SERVER_URL", "GRR_URL", "PRISMIC_URL", "KOHA_URL", "AGENDA_ICS_URL", "HORAIRES_ICS_URLS", "SMTP_HOST", "SMTP_FROM", "MYSQL_HOST", "IDS_AREA_MATERIEL", "IDS_AREA_SALLES", "SUGGESTION_ALIAS_MAIL_CATEGORIES", "RDV_ALIAS_MAIL_DESTINATAIRE", "DEMANDE_REFERENCE_ALIAS_MAIL" ].filter(env => !process.env.hasOwnProperty(env))
if(missing_env_vars.length)
  throw "Variables d'environnement non déclarées dans .env : " + missing_env_vars.join(", ");

//Modules basiques pour faire tourner le serveur
const express = require('express');
const path = require('path');
const logger = require('morgan');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const semver = require('semver');
const pkg = require('./package.json');

//Modules complémentaires
const log = require('metalogger')();

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Chargement des APIs

app.use((req, res, next) => {
  log.debug(req.headers);
  
  let app_version = req.headers['X-LCA-app-version'],
      content_type = req.headers['accept-encoding'];
  //Filtrage uniquement sur les appels aux webservices, pas les trucs statiques 
  // images proxiées par ex ont un content type :  'accept-encoding': 'gzip, deflate',
  if (content_type == "application/json" && (!app_version || !semver.satisfies(app_version, pkg.app_compatibility))){
    log.debug(`semver.satisfies(${app_version}, ${pkg.app_compatibility}) : `, semver.satisfies(app_version, pkg.app_compatibility));
    
    return res.status(417).json({ code: 'error_maj_app', plaintext: 'Vous devez mettre à jour votre application pour continuer' })
  } 
  else 
    next()
});
// Variables pour pug
app.use((req, res, next) => {
  res.locals.ROOT_URL = process.env.ROOT_URL; //Url racine pour tous les fichiers jade
  res.locals.VERSION = process.env.VERSION; //Numéro de version pour invalidation du cache utilisateur
  res.locals.VERSION_CACHE = "?v="+process.env.VERSION; //String pour utilisation directe dans templates
  res.locals.NODE_ENV = process.env.NODE_ENV; //String pour utilisation directe dans templates
  res.locals.SUPPORT_TECHNIQUE_MAIL = process.env.SUPPORT_TECHNIQUE_MAIL; //Adresse e-mail affichée en cas d'erreur de connexion shibboleth, pour que l'utilisateur contacte le support en cas d'erreur
  res.locals.moment = require("moment");
  res.locals.default_meta = {
    seo_title: process.env.SEO_TITLE,
    seo_description: process.env.SEO_DESCRIPTION,
    seo_image: process.env.SEO_IMAGE
  }
  next();
});

app.use(require('./api/shibboleth'));
app.use(require('./api/prismic'));
app.use(require('./api/grr'));
app.use(require('./api/koha'));
app.use(require('./api/ics'));
app.use(require('./api/mail'));

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    log.error(err);
    res.sendStatus(err.status || 500);
  });
}

app.listen(app.get('port'), function() {
  log.notice('Express server listening on port ' + app.get('port'));
});

module.exports = app;
