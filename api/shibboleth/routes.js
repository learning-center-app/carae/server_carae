//Modules indispensables
const express = require('express');
const request = require('request');
const log = require('metalogger')();
//Whitelist de nom de domaines, utilie en cas de WAYF
const SHIBBOLETH_DN_WHITELIST = process.env.SHIBBOLETH_DN_WHITELIST ? JSON.parse(process.env.SHIBBOLETH_DN_WHITELIST) : null;
const SHIBBOLETH_TEST_USER = process.env.SHIBBOLETH_TEST_USER;
//API éventuelle de récupération de numéro de carte via UID
const SHIBBOLETH_RUCCARTE_API = process.env.SHIBBOLETH_RUCCARTE_API;

const app = express.Router();
//Helper ISO-8859-1 to UTF8
function ISOtoUTF8(isoString) {
  //Si le string passé n'est pas en ISO, mais en UTF8 -https://stackoverflow.com/a/26900132/1437016
  try {
    //https://stackoverflow.com/a/22594794/1437016
    return decodeURIComponent(escape(isoString))
  }
  catch(e) {
    //On le renvoie tel quel
    return isoString;
  }
}

//Modules spécifiques
//Authentification de l'utilisateur via Shibboleth / Apache
app.get('/api/shibboleth/login', (req, res) => {
  //Si on force une utilisation avec un utilisateur de test, on se stop ici
  if(SHIBBOLETH_TEST_USER)
    return res.render('callback_sso', { utilisateur: SHIBBOLETH_TEST_USER });

  if (!req.headers.eppn) {
    return res.render('callback_sso', {
      error : 'Vous avez annulé la connexion, ou votre établissement n\'est pas conforme à la norme requise.'
    });
  }
  var shibboleth_user = {
    eppn: req.headers.eppn,
    email : req.headers.mail,
    userId : req.headers.uid || req.headers.eppn.match(/(.*?)@/)[1],
    //Certains providers envoient en camelCase, d'autres non...
    name : ISOtoUTF8(req.headers.displayName || (req.headers.givenName || req.headers.givenname || '').split(";").pop() + ' ' + (req.headers.sn || '').split(";").pop()),
    prenom: req.headers.givenName || req.headers.givenname, //Pour upsert db locale GRR
    nom: req.headers.sn, //Pour upsert db locale GRR
    id_koha: req.headers.ruccarte,
    formation:  req.headers.rucOptSIS,
    service:  req.headers.supannEntiteAffectationPrincipale,
    organization : req.headers.mail.match(/@(.*?)\./)[1].replace("-"," ")
  }
  // log.warning("shibboleth_user : ", shibboleth_user)
  // log.warning("req.headers : ", req.headers)
  //S'il y a une whitelist, on vérifie que le nom de domaine est bien autorisé
  if (SHIBBOLETH_DN_WHITELIST ? SHIBBOLETH_DN_WHITELIST.indexOf(shibboleth_user.email.split("@")[1]) >= 0 : true) {
    //S'il faut récupérer le numéro de carte via une API
    if (SHIBBOLETH_RUCCARTE_API) {
      request.get(SHIBBOLETH_RUCCARTE_API.replace("{uid}", shibboleth_user.userId), function (error, response, body) {
        if (error && !body)
          log.error(error);
        else 
          shibboleth_user.id_koha = body;
        return res.render('callback_sso', {
          utilisateur: JSON.stringify(shibboleth_user)
        });
      })
    }
    //Sinon on roule
    else
      res.render('callback_sso', {
        utilisateur : JSON.stringify(shibboleth_user)
      });
  }
  else {
    res.render('callback_sso', {
      error: "Cette application est reservée aux personnels et étudiants de l'École des Mines de Saint-Étienne. Vous n'êtes pas autorisé·e à vous connecter."
    });
  }
});

module.exports = app;

/** Exemple de retour Shibboleth

Array (
[Shib-Cookie-Name] =>
[Shib-Session-ID] => _26f4337291d28530353735158ea59d18
[Shib-Session-Index] => _96a0a2d2aec6cc79a62c5972920a0d36
[Shib-Identity-Provider] => https://idp.imt-atlantique.fr/idp/shibboleth
[Shib-Authentication-Method] => urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport
[Shib-Authentication-Instant] => 2018-10-02T10:23:16.790Z
[Shib-AuthnContext-Class] => urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport
[Shib-AuthnContext-Decl] =>
[Shib-Assertion-Count] =>
[Shib-Handler] => https://podcast.mines-nantes.fr/Shibboleth.sso
[targeted-id] =>
[persistent-id] => https://idp.imt-atlantique.fr/idp/shibboleth!https://podcast.mines-nantes.fr/shibboleth!tHv9x2mq8KTX7hbDz1va4niWIJ8=
[eppn] => mjugan12@emn.fr
[affiliation] =>
[unscoped-affiliation] => affiliate
[primary-affiliation] => affiliate
[entitlement] =>
[nickname] =>
[primary-orgunit-dn] =>
[orgunit-dn] =>
[org-dn] =>
[supannActivite] =>
[supannAutreTelephone] =>
[supannCivilite] =>
[supannEmpId] =>
[supannEntiteAffectation] =>
[supannEntiteAffectationPrincipale] =>
[supannEtablissement] =>
[supannRoleEntite] =>
[supannRoleGenerique] =>
[supannCodeINE] =>
[supannEtuAnneeInscription] =>
[supannEtuCursusAnnee] =>
[supannEtuDiplome] =>
[supannEtuElementPedagogique] =>
[supannEtuEtape] =>
[supannEtuId] =>
[supannEtuInscription] =>
[supannEtuRegimeInscription] =>
[supannEtuSecteurDisciplinaire] =>
[supannEtuTypeDiplome] =>
[supannListeRouge] =>
[supannMailPerso] =>
[uid] => mjugan12
[cn] =>
[sn] => JUGANAIKLOO
[givenName] => Maen
[displayName] => Maen JUGANAIKLOO
[mail] => maen@pairform.fr
[preferredLanguage] =>
[telephoneNumber] =>
[title] =>
[facsimileTelephoneNumber] =>
[postalAddress] =>
[street] =>
[postOfficeBox] =>
[postalCode] =>
[st] =>
[l] =>
[o] =>
[ou] =>
[Shib-Application-ID] => default
[REMOTE_USER] => mjugan12@emn.fr
)
*/
