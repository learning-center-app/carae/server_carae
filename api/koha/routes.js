const request = require('request');
const moment = require('moment');

//Modules indispensables
const express = require('express');
const log = require('metalogger')();
const cheerio = require('cheerio');

const app = express.Router();

//Récupération des variables d'environnement
const { KOHA_VERSION, KOHA_URL, KOHA_REFERER_URL, KOHA_AUTHWALL_LOGIN, KOHA_AUTHWALL_PASSWORD, KOHA_BARCODE_PREFIX, KOHA_BARCODE_SUFFIX } = process.env;

let error_codes;
try {
  error_codes = require(`./error_codes/${KOHA_VERSION}.json`)
}
catch (e) {
  throw `Pas de fichier .json de codes d'erreur (./api/koha/error_codes/*.json) correspondant à la version KOHA indiquée dans .env (${KOHA_VERSION}).`
}

/**
Route /api/koha/book
Réservation d'un bouquin pour 6 mois

IN
- barcode (string) : 12345
- utilisateur.id_koha (string)
- confirmed 1 || null

OUT

200 - OK
  { book_title : error_msg, book_authors : error_reason }

401 - Unauthorized
  "err_id_invalide": {
    "title": "Attention",
    "body": "Vous devez renseigner votre code de carte avant de pouvoir réserver un livre.",
  },

403 - Forbidden
  "err_pret_deja_ok": {
    "title": "Prêt déjà fait",
    "body": "Ce livre est déjà emprunté en votre nom.",
  },

403 - Forbidden - Ne peut pas car situation irrégulière (a des livres en retard, peut pas réserver)
  "err_pret_interdit": {
    "title": "Prêt de livre bloqué",
    "body": "Votre situation ne permet pas de prêter un livre avec l’application. Auriez-vous un livre en retard ? Contactez un membre de l’équipe CARÆ.",
  },

404 - Not found
  "err_code_invalide": {
    "title": "Code barre non reconnu",
    "body": "Vous n’avez sans doute pas scanné le bon code barre. Il est généralement au début du livre, ou sur la page de couverture. En cas de doute, contactez un membre de l’équipe CARÆ.",
  },

409 - Conflict
  "err_pret_autre": {
    "title": "Livre emprunté par un tiers",
    "body": "Ce livre est déjà emprunté par une autre personne. Avez-vous scanné le bon code barre ? Contactez un membre de l’équipe CARÆ.",
  },

*/
app.post('/api/koha/book', (req, res, next) => {
  // console.log(req.body);
  if (!req.body.barcode) {
    //Forbidden
    return res.status(404).json({
      error_code : "err_code_invalide"
    });
  }
  if (!req.body.utilisateur.id_koha) {
    //Unauthorized
    return res.status(401).json({
      error_code : "err_id_invalide"
    });
  }
  const barcode_final = `${KOHA_BARCODE_PREFIX || ""}${req.body.barcode}${KOHA_BARCODE_SUFFIX || ""}`;

  let body = {
    op : 'checkout',
    barcode: barcode_final,
    patronid : req.body.utilisateur.id_koha,
    //Pour renouveler une réservation, passer 1 à koha
    confirmed: req.body.confirmed,
    //S'il y a une authentification à passer, parce que page protégée par authwall
    ...(KOHA_AUTHWALL_LOGIN && KOHA_AUTHWALL_PASSWORD ? {
      userid: KOHA_AUTHWALL_LOGIN,
      password: KOHA_AUTHWALL_PASSWORD,
    } : {})
  };
  const options = { 
    url: KOHA_URL, 
    form: body,
    headers: { 
      //Francais en priorité, pour le parse des messages d'erreur.
      "Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
      //Si la page est censée être intégrée dans un autre site en iframe, on trompe le serveur en passant un referer
      ...(KOHA_REFERER_URL ? { 'Referer': KOHA_REFERER_URL,  } : {}),
    }
  }
  request.post(options, function callback(error, response, body) {
    if (error) {
      log.error(error);
      return res.json(error);
    }

    // log.debug('body:', body);
    const $ = cheerio.load(body);
    //log.debug($('.alert').html());


    //Check des messages d'erreurs
    //Koha 3.0600107, pas de masthead
    //Koha 19.1112000, "Exemplaire prêté" dans un .alert-info quand prêt réussi. A exclure de l'analyse d'erreur donc
    let error_msg = $('.alert:not(.alert-info)').children('h3').text();
    let error_reason = $('.alert:not(.alert-info)').children('p').text();

    //S'il n'y a pas de message d'erreur
    if (!error_msg && !error_reason){
      //On considère que c'est good (y'a pas de confirmation quand c'est bon)
      //On va chercher dans le tableau des bouquins réservés le dernier titre
      //ATTENTION : malgré que dans l'interface web, la première ligne est le plus récent bouquin,
      // dans le code html source, la première ligne est le plus ancien :
      // le sort est certainement fait en JS après chargemetn du DOM.
      // Il faut donc récupérer le dernier au lieu du premier
      // ATTENTION BIS : 3.0600107 - le dernier livre est en dernier dans le code source, commentaire précédent plus valable.
      // ATTENTION BIS BIS : 19.1112000 - le dernier livre est en premier dans le code source, commentaire précédent plus valable.
      let last_book = $('#loanTable tbody').children('tr').first(),
          last_book_title = last_book.find('strong').text(),  //<strong>D&#xE9;couvrez vos points forts</strong>
          last_book_authors = last_book.find('.item-details').text(),  // <span class="item-details">Marcus Buckingham &amp; Donald Clifton</span>
        last_book_return_date = last_book.children('td:nth-child(4)').text(); // <td>09/06/2021</td> Pour 3.0600107 - 3e TD au lieu du 4e

      log.debug({
        book_title: last_book_title,
        book_authors: last_book_authors,
        book_return_date: last_book_return_date,
        book_all_infos: last_book.text(),
        barcode_asked: barcode_final,
        userId: req.body.utilisateur.userId,
        id_koha: req.body.utilisateur.id_koha,
      })

      res.status(200).json({
        book_title : last_book_title,
        book_authors : last_book_authors,
        book_return_date : last_book_return_date,
      });
    }
    else {
      let error = error_codes.find(error_code => {
        return error_reason.match(new RegExp(error_code.error_reason_match, "gmi"));
      }) || { error_code: "err_pret_interdit", status: 403 }

      //Exception pour une erreur de renouvellement de prêt, 
      //qui affiche exactement la même erreur que lorsqu'un prêt est déjà fait en son nom
      //https://gitlab.com/learning-center-app/carae/app_carae/-/issues/14#note_828283779
      if (error.error_code === "err_pret_deja_ok" && req.body.confirmed) {
        //Si le flag confirmed est à 1, on est dans le cas d'un renouvellement
        //Override du code d'erreur pour affichage d'un autre texte dans l'app
        error.error_code = "err_renouvellement_impossible"
      }

        
      log.debug({ 
        ...error,
        error_reason,
        barcode : barcode_final,
        userId : req.body.utilisateur.userId,
        id_koha : req.body.utilisateur.id_koha,
      })
      res.status(error.status).json({
        error_code : error.error_code
      });
    }
  });
});


app.get('/api/koha/books', (req, res, next) => {

  let body = {
    op: 'login',
    patronid: req.param("id_koha"),
    //S'il y a une authentification à passer, parce que page protégée par authwall
    ...(KOHA_AUTHWALL_LOGIN && KOHA_AUTHWALL_PASSWORD ? {
      userid: KOHA_AUTHWALL_LOGIN,
      password: KOHA_AUTHWALL_PASSWORD,
    } : {})
  };

  const options = {
    url: KOHA_URL,
    form: body,
    headers: {
      //Francais en priorité, pour le parse des messages d'erreur.
      "Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
      //Si la page est censée être intégrée dans un autre site en iframe, on trompe le serveur en passant un referer
      ...(KOHA_REFERER_URL ? { 'Referer': KOHA_REFERER_URL, } : {}),
    }
  }
  request.post(options, function callback(error, response, body) {
    if (error) {
      log.error(error);
      return res.json(error);
    }

    // log.debug('body:', body);
    const $ = cheerio.load(body);

    //ATTENTION : malgré que dans l'interface web, la première ligne est le plus récent bouquin,
    // dans le code html source, la première ligne est le plus ancien :
    // le sort est certainement fait en JS après chargemetn du DOM.
    // Il faut donc récupérer le dernier au lieu du premier
    // ATTENTION BIS : 3.0600107 - le dernier livre est en dernier dans le code source, commentaire précédent plus valable.
    // ATTENTION BIS BIS : 19.1112000 - le dernier livre est en premier dans le code source, commentaire précédent plus valable.
    let books = $('#loanTable tbody').children('tr');
    
    let books_json = books.toArray().map(item => {
      const $item = $(item),
            /* Contenu de la 5e cellule : 
              <form action="/cgi-bin/koha/sco/sco-main.pl" method="post">
                <input type="hidden" name="patronid" value="1159376134816640">
                <input type="hidden" name="barcode" value="2075431137">
            */
            barcode = $item.find('td:nth-child(5)').find("input[name=barcode]").val(),
            // Renouvelable : <input class="btn renew" name="confirm  " type="submit" value="Renouveler l&apos;exemplaire">
            // Non renouvalble : <span>Aucun renouvellement autoris&#xE9;</span>
            renewable = $item.find('td:nth-child(5)').find("span").length ? false : true;
      return {
        date: $item.find('td:nth-child(1)').text(),
        title: $item.find('td:nth-child(2) strong').text(),
        author: $item.find('td:nth-child(2) .item-details').text(),
        book_number: $item.find('td:nth-child(3)').text(),
        return_date: $item.find('td:nth-child(4)').text(),
        barcode,
        renewable,
      }
    })
    //Tri des livres par date de retour la plus proche
    if(books_json.length)
      books_json.sort((book1, book2) => moment(book1.return_date, 'DD/MM/YYYY').diff(moment(book2.return_date, 'DD/MM/YYYY')))
    
    return res.json(books_json);
  });
});

module.exports = app;
