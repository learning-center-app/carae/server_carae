const request = require('request');

const log = require('metalogger')();
const cheerio = require('cheerio');
const dotenv = require('dotenv').load();

const { KOHA_VERSION, KOHA_TEST_BARCODE, KOHA_TEST_ID_KOHA, KOHA_URL, KOHA_REFERER_URL, KOHA_AUTHWALL_LOGIN, KOHA_AUTHWALL_PASSWORD, KOHA_BARCODE_PREFIX, KOHA_BARCODE_SUFFIX } = process.env;
let error_codes;
try {
  error_codes = require(`./error_codes/${KOHA_VERSION}.json`)
}
catch (e) {
  throw `Pas de fichier .json de codes d'erreur (./api/koha/error_codes/*.json) correspondant à la version KOHA indiquée dans .env (${KOHA_VERSION}).`
}
const barcode_final = `${KOHA_BARCODE_PREFIX || ""}${KOHA_TEST_BARCODE}${KOHA_BARCODE_SUFFIX || ""}`;

let body = {
  op : 'checkout',
  barcode: barcode_final,
  patronid : KOHA_TEST_ID_KOHA,
  //S'il y a une authentification à passer, parce que page protégée par authwall
  ...(KOHA_AUTHWALL_LOGIN && KOHA_AUTHWALL_PASSWORD ? {
    userid: KOHA_AUTHWALL_LOGIN,
    password: KOHA_AUTHWALL_PASSWORD,
  } : {})
};
const options = { 
  url: KOHA_URL, 
  form: body,
  headers: { 
    //Francais en priorité, pour le parse des messages d'erreur.
    "Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
    //Si la page est censée être intégrée dans un autre site en iframe, on trompe le serveur en passant un referer
    ...(KOHA_REFERER_URL ? { 'Referer': KOHA_REFERER_URL,  } : {}),
  }
}
request.post(options, function callback(error, response, body) {
  if (error) {
    log.error(error);
  }

  // log.debug('body:', body);
  const $ = cheerio.load(body);
  //log.debug($('.alert').html());


  //Check des messages d'erreurs
  //Koha 3.0600107, pas de masthead
  //Koha 19.1112000, "Exemplaire prêté" dans un .alert-info quand prêt réussi. A exclure de l'analyse d'erreur donc
  let error_msg = $('.alert:not(.alert-info)').children('h3').text();
  let error_reason = $('.alert:not(.alert-info)').children('p').text();
  //S'il n'y a pas de message d'erreur
  if (!error_msg && !error_reason) {
    //On considère que c'est good (y'a pas de confirmation quand c'est bon)
    //On va chercher dans le tableau des bouquins réservés le dernier titre
    //ATTENTION : malgré que dans l'interface web, la première ligne est le plus récent bouquin,
    // dans le code html source, la première ligne est le plus ancien :
    // le sort est certainement fait en JS après chargemetn du DOM.
    // Il faut donc récupérer le dernier au lieu du premier
    // ATTENTION BIS : 3.0600107 - le dernier livre est en dernier dans le code source, commentaire précédent plus valable.
    // ATTENTION BIS BIS : 19.1112000 - le dernier livre est en premier dans le code source, commentaire précédent plus valable.
    let last_book = $('#loanTable tbody').children('tr').first(),
      last_book_title = last_book.find('strong').text(),  //<strong>D&#xE9;couvrez vos points forts</strong>
      last_book_authors = last_book.find('.item-details').text(),  // <span class="item-details">Marcus Buckingham &amp; Donald Clifton</span>
      last_book_return_date = last_book.children('td:nth-child(3)').text(); // <td>09/06/2021</td> 3.0600107 - 3e TD au lieu du 4e

    log.warning({
      book_title: last_book_title,
      book_authors: last_book_authors,
      book_return_date: last_book_return_date,
      book_all_infos: last_book.text(),
      barcode_asked: barcode_final,  
    })
  }
  else {
    let status;

    let error = error_codes.find(error_code => {
      return error_reason.match(new RegExp(error_code.error_reason_match, "gmi"));
    }) || { error_code : "err_pret_interdit", status : 403 }
    
    log.warning({
      ...error,
      error_reason,
      barcode: barcode_final,
      id_koha: KOHA_TEST_ID_KOHA,
    })
  }
});