//Modules indispensables
const express = require('express');
const log = require('metalogger')();

const app = express.Router();

//Modules spécifiques
const mysql = require('mysql2');
const DAO = require('./DAO');


// Create the connection pool. The pool-specific settings are the defaults
if (!process.env.MYSQL_HOST) {
  throw "Aucun hote mysql renseigné dans .env ; éventuellement, c'est qu'il n'y a pas de fichier d'environnement .env détecté : copiez .env.example en .env, puis ajustez les paramètres."
}
const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  port: process.env.MYSQL_PORT,
  database: process.env.MYSQL_DATABASE,
  waitForConnections: process.env.MYSQL_WAITFORCONNECTIONS,
  connectionLimit: process.env.MYSQL_CONNECTIONLIMIT,
  queueLimit: process.env.MYSQL_QUEUELIMIT,
})
//Suppression de only_full_group_by (defaut MySQL >= 5.7.5), qui cause cette erreur dans la récupération des résa :
//  "sqlMessage": "Expression #2 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'grr.gr.statut_room' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by"
//https://stackoverflow.com/a/36033983/1437016
.on('connection', (conn) => conn.query("SET SESSION sql_mode =(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"));

const promisePool = pool.promise();

const ID_SALLES_COLLABORATION_NANTES = 1;
const ID_SALLES_COLLABORATION_BREST = 5;

//Exemple : const [rows, fields] = await promisePool.execute('SELECT * FROM `grr_area` WHERE `name` = ? AND `age` > ?', ['Morty', 14]);

require('./common')(app, DAO, promisePool)
require('./materiel')(app, DAO, promisePool)
require('./salles')(app, DAO, promisePool)
require('./utilisateur')(app, DAO, promisePool)


module.exports = app;
