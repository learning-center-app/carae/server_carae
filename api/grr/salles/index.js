const log = require('metalogger')();

module.exports = function (app, DAO, promisePool) {
  //Récupération de la liste des campus
  app.get('/api/grr/campus', async (req, res) => {
    const [rows, fields] = await promisePool.query('SELECT * FROM `grr_sites`');
    res.json(rows);
  });


  /**
    Récupération de toutes les salles des campus
    out :
      - liste des salles (array)
  */
  app.get('/api/grr/salles', async (req, res) => {
    const [rows, fields] = await promisePool.query(DAO.getSallesCollaboration);
    res.json(rows);
  });

  /**
    Récupération de toutes les réservations de salles
    out :
      - liste des réservations (array)
  */
  app.get('/api/grr/salles/reservations', async (req, res) => {
    try {
      const [rows, fields] = await promisePool.query(DAO.getReservationsSalles);
      res.json(rows);
    } catch (e) {
      log.error(e);
      res.json([]);
    }
  });

}
