const log = require('metalogger')();

const moment = require('moment');
//Modules spécifiques pour l'envoi de mail, en cas de ressource nécessitant une modération
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport(process.env.SMTP_HOST)
const GRR_MODERATION_RESERVATION_MAIL = process.env.GRR_MODERATION_RESERVATION_MAIL
const GRR_DEFAULT_RESERVATION_TYPE = process.env.GRR_DEFAULT_RESERVATION_TYPE
const GRR_RESERVATION_RESSOURCE_EMPRUNTEE = process.env.GRR_RESERVATION_RESSOURCE_EMPRUNTEE || false
const GRR_DEFAULT_RESERVATION_RETARD_EMAIL = process.env.GRR_DEFAULT_RESERVATION_RETARD_EMAIL || false

// Pour le forward des images de GRR
var http_proxy = require('http-proxy');
// Ignore du path, on le reconstruit nous même
// https://www.npmjs.com/package/http-proxy#options
var image_proxy = http_proxy.createProxyServer({ignorePath: true, secure: false});

module.exports = function (app, DAO, promisePool) {
  /**
  Création d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.put('/api/grr/reservation', async (req, res) => {
    /*
      Etat actuel des types
      {
      "ZA": "Travail seul ou en groupe",
      "ZB": "Enseignement",
      "ZC": "Enseignement des langues",
      "ZD": "Tutorat",
      "ZE": "Enseignement à distance",
      "ZF": "Création de vidéos",
      "ZG": "Autre"

    }
    */
    log.debug(req.body);
    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.putReservation, [
        req.body.start_time, //Date de début
        req.body.end_time, //Date de fin
        req.body.room_id, //Identifiant de la salle
        req.body.user.userId, //create_by - User ID (mjugan12, dcance07…)
        req.body.user.userId, //Beneficiaire - User ID (mjugan12, dcance07…)
        // req.body.user.userId, //Beneficiaire_ext - User ID (mjugan12, dcance07…)
        req.body.user.name, //Nom complet utilisateur
        req.body.type || GRR_DEFAULT_RESERVATION_TYPE,   //Type
        req.body.description,   //Description libre
        //Si on n'automatise pas les flages de ressource empruntée, on met "-" pour dire que la ressource n'est pas encore empruntée, le flag est updaté manuellement par l'équipe (voir #8)
        //Si la ressource est suivie (="y" si réservé sans mail, ="e" si envoi de mail retard, ="-" si ressource restituée (ou si pas suivie)
        GRR_RESERVATION_RESSOURCE_EMPRUNTEE ? (req.body.room_active_ressource_empruntee == "y" ? (GRR_DEFAULT_RESERVATION_RETARD_EMAIL ? "e" : "y") : "-") : "-", 
        req.body.moderate || 0   //Est-ce que la réservation a besoin d'être modérée par un administrateur (1 = oui, 0 = c'est bon). 0 par défaut
      ]).catch((err)=>{throw err});
      //S'il y a un besoin de modération
      if (req.body.moderate == 1) {
        //On notifie aux administrateurs par l'alias GRR_MODERATION_RESERVATION_MAIL
        notifyAdminForGRRModeration(req.body, insertId);
      }
      if (affectedRows == 1) {
        log.notice("Insertion d'une réservation ")
        const [rows, fields] = await promisePool.execute(DAO.getReservation, [insertId]);
        res.json(rows[0]);
      }
      else {
        throw {code:'NO_INSERT', msg:'Réservation impossible.', params: req.body}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });



  /**
  Edit d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.post('/api/grr/reservation', async (req, res) => {
    log.debug(req.body);
    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.updateReservation, [
        req.body.start_time, //Identifiant de la salle
        req.body.end_time, //Identifiant de la salle
        req.body.id, //Identifiant de la salle
      ]).catch((err)=>{throw err});

      if (affectedRows == 1) {
        log.notice("Modification d'une réservation", affectedRows)
        const [rows, fields] = await promisePool.execute(DAO.getReservation, [req.body.id]);
        res.json(rows[0]);
      }
      else {
        throw {code:'NO_UPDATE', msg:'Impossible de modifier cette réservation.'}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });


  /**
  Suppression d'une réservation
  in :
  - room (object)
  out :
  - room (object) : Résa créée
  */
  app.delete('/api/grr/reservation', async (req, res) => {
    log.debug(req.query);
    if (req.query.start_time <= (Date.now() / 1000))
      return res.status(401).json({code:'NO_DELETE', error:'Vous ne pouvez pas supprimer une réservation déjà commencée ou passée.'});

    try {
      const [{affectedRows, insertId}] = await promisePool.execute(DAO.deleteReservation, [
        req.query.id, //Identifiant de la salle
      ]).catch((err)=>{throw err});

      if (affectedRows == 1) {
        log.notice("Suppression d'une réservation", affectedRows)
        res.json(req.query);
      }
      else {
        throw {code:'NO_DELETE', error:'Impossible de supprimer cette réservation.'}
      }
    } catch (e) {
      log.error(e);
      res.status(500).json(e);
    }
  });

  app.get('/api/grr/image/:image', (req, res) => {
    console.log(`${process.env.GRR_URL_PROXY || process.env.GRR_URL}images/${req.params.image}`);
    
    image_proxy.web(req, res, {
      target: `${process.env.GRR_URL_PROXY || process.env.GRR_URL}images/${req.params.image}`
    });
  })
}


function notifyAdminForGRRModeration(data, insertId, callback) {
  //Récupération du bon mail
  if (!GRR_MODERATION_RESERVATION_MAIL) {
    throw `Aucun email défini dans la variable d'environnement GRR_MODERATION_RESERVATION_MAIL - alors que la ressource #${data.room_id} a été reservée`
  }
  const options = {
    to: GRR_MODERATION_RESERVATION_MAIL,
    from: process.env.SMTP_FROM,
    subject: `Demande de validation d’une réservation - ${data.campus_name}`,
    text: 
`Depuis l’application mobile, une demande de réservation de la ressource ${data.room_name} (${data.room_id}), du ${moment(data.start_time * 1000).format("DD/MM/YYYY - HH[h]mm")} au ${moment(data.end_time * 1000).format("DD/MM/YYYY - HH[h]mm")}, par ${data.user.name} nécessite une validation.
Si vous êtes gestionnaire de cette ressource, allez sur - ${process.env.GRR_URL}view_entry.php?id=${insertId} - pour valider ou refuser la demande de réservation.

Attention : pensez à vous authentifier auparavant sur ${process.env.GRR_URL} afin de pouvoir intervenir.
`
  };

  log.debug(options);

  transporter.sendMail(options, (err) => {
    if (err) {
      log.error(err);
    }
    typeof callback == "function" && callback(err);
  });
}