const log = require('metalogger')();

module.exports = function (app, DAO, promisePool) {
  /**
    Récupération de toutes le matos d'un campus
    out :
      - liste du materiel (array)
  */
  app.get('/api/grr/materiel', async (req, res) => {
    const [rows, fields] = await promisePool.query(DAO.getMateriel);
    res.json(rows);
  });

  /**
    Récupération de toutes les réservations de materiel
    out :
      - liste des réservations (array)
  */
  app.get('/api/grr/materiel/reservations', async (req, res) => {
    try {
      const [rows, fields] = await promisePool.query(DAO.getReservationsMateriel);
      res.json(rows);
    } catch (e) {
      log.error(e);
      res.json([]);
    }
  });
}
