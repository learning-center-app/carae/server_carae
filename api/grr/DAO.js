var DAO = {};

const IDS_AREA_MATERIEL = JSON.parse(process.env.IDS_AREA_MATERIEL);
const IDS_AREA_SALLES = JSON.parse(process.env.IDS_AREA_SALLES);
const BLACKLIST_SALLE_NAME = JSON.parse(process.env.BLACKLIST_SALLE_NAME);

DAO.getSallesCollaboration =
`SELECT gr.*, gs.sitename as 'campus_name', gs.id as 'campus_id',
 IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image',
 ga.morningstarts_area AS 'min_hour',
 ga.eveningends_area AS 'max_hour'
 FROM grr_room gr
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ${(IDS_AREA_SALLES && IDS_AREA_SALLES.length) ? ` ga.id IN (${IDS_AREA_SALLES.join(",")})` : "TRUE"}
  ${(BLACKLIST_SALLE_NAME && BLACKLIST_SALLE_NAME.length) ? `AND gr.room_name NOT RLIKE "${BLACKLIST_SALLE_NAME.join("|")}"`: ""}
 ORDER BY gr.order_display ASC`;

DAO.getReservationsSalles =
`SELECT 
  IF(gr.statut_room = 0, -gr.id, ge.id) as 'id', 
  IF(gr.statut_room = 0, "Non réservable", ge.create_by) as 'create_by',
  IF(gr.statut_room = 0, "Non réservable", ge.beneficiaire) as 'beneficiaire',
  IF(gr.statut_room = 0, gr.id, ge.room_id) as 'room_id', 
  IF(gr.statut_room = 0, 0, ge.timestamp) as 'timestamp', 
  ge.moderate,
  gr.statut_room as 'reservable',
  ge.statut_entry,
  IF(gr.statut_room = 0, 0, ge.start_time) as 'start_time',
  IF(gr.statut_room = 0, 9999999999, ge.end_time) as 'end_time',
  gr.room_name AS 'salle', 
  gs.sitename as 'campus_name', 
  gs.id as 'campus_id',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'

FROM grr_entry ge
  JOIN grr_room gr ON gr.id = ge.room_id
  JOIN grr_area ga ON ga.id = gr.area_id
  JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
  JOIN grr_site gs ON gs.id = gjsa.id_site
  
WHERE ${(IDS_AREA_SALLES && IDS_AREA_SALLES.length) ? `ga.id IN (${IDS_AREA_SALLES.join(",")})` : "TRUE"}
  AND (
    (
      (
        ge.start_time >= (UNIX_TIMESTAMP() - 3600 * 24) AND
        ge.start_time <= (UNIX_TIMESTAMP() + (3600 * 24 * 60))
      )
      OR ge.end_time >= (UNIX_TIMESTAMP() - 3600 * 24)  
    ) 
    ${(BLACKLIST_SALLE_NAME && BLACKLIST_SALLE_NAME.length) ? `AND gr.room_name NOT RLIKE "${BLACKLIST_SALLE_NAME.join("|")}"`: ""}
  )
  OR (gr.statut_room = 0)

GROUP BY id
ORDER BY ge.start_time ASC;`;


 DAO.getReservation =
 `SELECT ge.*, gr.room_name AS 'salle', gr.room_name AS 'materiel', gs.sitename as 'campus_name', gs.id as 'campus_id',
  ga.area_name as 'type_materiel',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'
  FROM grr_entry ge
  JOIN grr_room gr ON gr.id = ge.room_id
  JOIN grr_area ga ON ga.id = gr.area_id
  JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
  JOIN grr_site gs ON gs.id = gjsa.id_site
  WHERE ge.id = ?
    ${(BLACKLIST_SALLE_NAME && BLACKLIST_SALLE_NAME.length) ? `AND gr.room_name NOT RLIKE "${BLACKLIST_SALLE_NAME.join("|")}"`: ""}
  ORDER BY ge.start_time ASC`;


/* MATOS */


DAO.getMateriel =
`SELECT gr.*,
  gs.sitename as 'campus_name', gs.id as 'campus_id',
  ga.area_name as 'type_materiel',
  ga.order_display as 'area_order_display',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'
 FROM grr_room gr
 JOIN grr_area ga ON ga.id = gr.area_id
 JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
 JOIN grr_site gs ON gs.id = gjsa.id_site
 WHERE ${(IDS_AREA_MATERIEL && IDS_AREA_MATERIEL.length) ? `ga.id IN (${IDS_AREA_MATERIEL.join(",")})` : "TRUE"}
 ORDER BY gr.order_display ASC`;

DAO.getReservationsMateriel =
`SELECT 
  IF(gr.statut_room = 0, -gr.id, ge.id) as 'id', 
  IF(gr.statut_room = 0, "Non réservable", ge.create_by) as 'create_by',
  IF(gr.statut_room = 0, "Non réservable", ge.beneficiaire) as 'beneficiaire',
  IF(gr.statut_room = 0, gr.id, ge.room_id) as 'room_id', 
  IF(gr.statut_room = 0, 0, ge.timestamp) as 'timestamp', 
  ge.moderate,
  gr.statut_room as 'reservable',
  ge.statut_entry,
  IF(gr.statut_room = 0, 0, ge.start_time) as 'start_time',
  IF(gr.statut_room = 0, 9999999999, ge.end_time) as 'end_time',
  gr.room_name AS 'salle', 
  gs.sitename as 'campus_name', 
  gs.id as 'campus_id',
  IF(gr.picture_room != '', CONCAT('${process.env.SERVER_URL}api/grr/image/', gr.picture_room), '') AS 'image'

FROM grr_entry ge
  JOIN grr_room gr ON gr.id = ge.room_id
  JOIN grr_area ga ON ga.id = gr.area_id
  JOIN grr_j_site_area gjsa ON gjsa.id_area = ga.id
  JOIN grr_site gs ON gs.id = gjsa.id_site
  
WHERE ${(IDS_AREA_MATERIEL && IDS_AREA_MATERIEL.length) ? `ga.id IN (${IDS_AREA_MATERIEL.join(",")})` : "TRUE"}
  AND (
    (
      (
        ge.start_time >= (UNIX_TIMESTAMP() - 3600 * 24) AND
        ge.start_time <= (UNIX_TIMESTAMP() + (3600 * 24 * 60))
      )
      OR ge.end_time >= UNIX_TIMESTAMP()
    )
  OR (gr.statut_room = 0)
  OR ((ge.statut_entry IN ("y", "e")) AND gr.active_ressource_empruntee = "y")
)

GROUP BY id
ORDER BY ge.start_time ASC;`;

/**
  start_time, //Date de début
  end_time, //Date de fin
  entry_type, //Parce que ça se répète pas : voir edit_entry_handler.php:627
  repeat_id, //Parce que ça se répète pas
  room_id, //Identifiant de la salle
  create_by, //User ID (mjugan12, dcance07…)
  beneficiaire, //User ID (mjugan12, dcance07…)
  beneficiaire_ext, //User ID (mjugan12, dcance07…)
  name, //Nom complet utilisateur
  type, // Réunion
  description, //Description libre
  statut_entry, //Etat de la résa (="y" si réservé sans mail, ="e" si envoi de mail retard, ="-" si ressource restituée)
  option_reservation, //Aucune idée de ce que c'est
  overload_desc, //Overload, ce sont les paramètres définis par l'admin
  moderate, //Est-ce que la réservation a besoin d'être modérée par un administrateur (1 = oui, 0 = c'est bon)
  jours, //Aucune idée de ce que c'est
  clef, //Aucune idée de ce que c'est
  courrier //Aucune idée de ce que c'est
*/
DAO.putReservation =
`
INSERT INTO grr_entry (
  start_time,
  end_time,
  entry_type,
  repeat_id,
  room_id,
  create_by,
  beneficiaire,
  beneficiaire_ext,
  name,
  type,
  description,
  statut_entry,
  option_reservation,
  overload_desc,
  moderate,
  jours,
  clef,
  courrier
)
VALUES (
  ?,
  ?,
  0,
  0,
  ?,
  ?,
  ?,
  "",
  ?,
  ?,
  ?,
  ?,
  -1,
  "",
  ?,
  0,
  0,
  0
)
`

DAO.updateReservation =
`
UPDATE grr_entry
SET
  start_time = ?,
  end_time = ?
WHERE id = ?
`

DAO.deleteReservation =
`
DELETE
FROM grr_entry
WHERE id = ?
`

DAO.upsertUtilisateur = 
`
  INSERT INTO grr_utilisateurs (
    login,
    nom,
    prenom,
    password,
    email,
    statut,
    etat,
    default_site,
    default_area,
    default_room,
    default_style,
    default_list_type,
    default_language,
    source
  ) VALUES (
    ?,
    ?,
    ?,
    '',
    ?,
    'utilisateur',
    'actif',
    '0',
    '0',
    '0',
    'default',
    'item',
    'fr',
    'ext'
  )
  ON DUPLICATE KEY UPDATE 
    nom = IF(nom = "", ?, nom),
    prenom = IF(prenom = "", ?, prenom),
    email = IF(email = "", ?, email)
`

module.exports = DAO;
