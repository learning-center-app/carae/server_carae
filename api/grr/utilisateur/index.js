const log = require('metalogger')();

module.exports = function (app, DAO, promisePool) {

  /**
    Upsert un utilisateur en base locale GRR
  */
  app.post('/api/grr/utilisateur', async (req, res) => {
    log.debug("/api/grr/utilisateur", req.body);
    let prenom, nom;

    if (!(req.body.nom || req.body.prenom) && req.body.name) {
      const match = req.body.name.match(/(\w.*?) (\w.*)$/mi)
      prenom = match[0];
      nom = match[1];
    }    
    const [rows, fields] = await promisePool.execute(DAO.upsertUtilisateur, [
      //Primary key
      req.body.userId, //User ID (mjugan12, dcance07…)
      //Pour la création
      req.body.nom, //Nom utilisateur
      req.body.prenom, //Prénom utilisateur
      req.body.email, //Prénom utilisateur
      //Pour l'update
      req.body.nom || nom, //Nom utilisateur
      req.body.prenom || prenom, //Prénom utilisateur
      req.body.email, //Prénom utilisateur
    ]).catch((err) => { throw err });
    res.json(rows);
  });
}
