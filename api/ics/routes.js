//Modules indispensables
const express = require('express');
const app = express.Router();

const log = require('metalogger')();

//Modules spécifiques
const request = require('request');
const async = require('async');
// const ical2json = require('ical2json');
const IcalExpander = require('ical-expander');

const moment = require('moment');
require('moment-round');

//Constantes
const HORAIRES_ICS_URLS = process.env.HORAIRES_ICS_URLS ? JSON.parse(process.env.HORAIRES_ICS_URLS) : [];
const AGENDA_ICS_URL = process.env.AGENDA_ICS_URL;
const HORAIRES_DAYS_TO_GENERATE = 10;
const HORAIRES_MAXIMUM_ITERATIONS = 300000;
const AGENDA_MONTHS = 12;
const AGENDA_MAX_ITERATIONS = 100;
const KEY_OUVERTURE = 'ouverture';
const KEY_PERMANENCE = 'permanence';
const KEY_ACCUEIL = 'accueil';

app.get('/api/ics/horaires', (req, res) => {

  async.transform(HORAIRES_ICS_URLS, function getJSONFromICS(accumulator, calendrier, index, callback) {
    request.get(calendrier.url, function (error, response, body) {
      if (error) {
        return callback(error);
      }
      const ical_expander = new IcalExpander({ ics : body, maxIterations: HORAIRES_MAXIMUM_ITERATIONS });

      let name_match = body.match(/X\-WR\-CALNAME\:(.*?)$/m),
          calendar_name = calendrier.nom || (name_match ? name_match[1] : 'Campus'),
          campus = {
            campus_id: calendrier.campus_id,
            campus_code: calendrier.campus_code,
            campus_name: calendar_name,
            subname: calendrier.subname,
            datas: []
          };

      const events = ical_expander.between(moment().floor(24, 'hours').toDate(), moment().floor(24, 'hours').add(HORAIRES_DAYS_TO_GENERATE, 'days').toDate());

      // calendrier.campus_id == 1 && log.warning(events.events);

      const mappedEvents = events.events.map(e => ({ startDate: e.startDate, endDate: e.endDate, nom: e.summary,  description: e.description }));
      const mappedOccurrences = events.occurrences.map(o => ({ startDate: o.startDate, endDate: o.endDate, nom: o.item.summary,  description: o.item.description }));
      const allEvents = [].concat(mappedEvents, mappedOccurrences);

      // log.debug(allEvents);
      allEvents.forEach(item => {
        // log.debug(item);
        let _date_start = moment().set({
              year : item.startDate.year,
              month : item.startDate.month-1,
              date : item.startDate.day,
              hour : item.startDate.hour,
              minute : item.startDate.minute
            }).locale('fr'),
            _date_end = moment().set({
              year : item.endDate.year,
              month : item.endDate.month-1,
              date : item.endDate.day,
              hour : item.endDate.hour,
              minute : item.endDate.minute
            }).locale('fr'),
            _hour_start = _date_start.format('Hmm'),
            _hour_end = _date_end.format('Hmm');
        //Recherche de la date
        var _day = campus.datas.find(day => {
          return day.date == _date_start.format('YYYY-MM-DD');
        })
        if (!_day){
          _day = {
            "date": _date_start.format('YYYY-MM-DD'),
            "horaires": [],
            "horaires_inter": []
          };
          campus.datas.push(_day);
        }
        //Une fois qu'on a le bon objet dans le tableau campus, on rajoute l'events
        //Le mot clé se trouve dans le champ "descrption", et l'override dans "nom"
        //Si y'a pas de description, on n'ajoute pas dans les horaires
        let _key = item.description ? item.description.toLowerCase().trim() : null;

        //Horaire principal
        if (_key == KEY_OUVERTURE) {
          _day.horaires.push({
            nom: "",
            deb: _hour_start,
            fin: _hour_end,
            description: "",
            couleur: ""
          })
        }
        //Horaires inter
        else if([KEY_PERMANENCE, KEY_ACCUEIL].indexOf(_key) != -1){
          //Priorité au nom, qui override le mot clé par défaut
          _day.horaires_inter.push({
            nom: item.nom || (_key == KEY_PERMANENCE ? 'Disponible' : KEY_ACCUEIL),
            // nom: (item.nom == KEY_PERMANENCE ? 'Disponible' : KEY_ACCUEIL),
            deb: _hour_start,
            fin: _hour_end,
            description: "",
            couleur: _key == KEY_PERMANENCE ? "rgb(0, 255, 255)" : "rgb(126, 255, 72)"
          })
        }
      })
      accumulator.push(campus);
      callback(null)
    });

  }, function callbackFinal(error, results) {
    if (error) {
      log.error(error);
      res.json(error);
    }
    else {
      res.json(results);
    }
  });
});

app.get('/api/ics/agenda', (req, res) => {
  request.get(AGENDA_ICS_URL, function (error, response, body) {
    if (error) {
      return res.json(error);
    }
    const ical_expander = new IcalExpander({ ics : body, maxIterations: AGENDA_MAX_ITERATIONS });

    let datas = [];

    const events = ical_expander.between(moment().toDate(), moment().add(AGENDA_MONTHS, 'month').toDate());

    const mappedEvents = events.events.map(e => ({ startDate: e.startDate, endDate: e.endDate, nom: e.summary, description: e.description, lieu: e.location }));
    const mappedOccurrences = events.occurrences.map(o => ({ startDate: o.startDate, endDate: o.endDate, nom: o.item.summary, description: o.item.description, lieu: o.item.location }));
    const allEvents = [].concat(mappedEvents, mappedOccurrences);

    allEvents.forEach(item => {
      // log.debug(item);
      let _date_start = moment().set({
            year : item.startDate.year,
            month : item.startDate.month-1,
            date : item.startDate.day,
            hour : item.startDate.hour,
            minute : item.startDate.minute
          }).locale('fr'),
          _date_end = moment().set({
            year : item.endDate.year,
            month : item.endDate.month-1,
            date : item.endDate.day,
            hour : item.endDate.hour,
            minute : item.endDate.minute
          }).locale('fr'),
          _hour_start = _date_start.format('Hmm'),
          _hour_end = _date_end.format('Hmm');

      //Recherche de la date
      var _month = datas.find(day =>  day.month == _date_start.month())
      if (!_month){
        _month = {
          "year": _date_start.year(),
          "month": _date_start.month(),
          "data": []
        };
        datas.push(_month);
      }
      _month.data.push({
        date: _date_start.format('YYYY-MM-DD'),
        titre: item.nom,
        lieu: item.lieu,
        deb: _hour_start,
        fin: _hour_end,
        description: item.description
      });
    })
    res.json(datas);
  });

});

/**
[
  {
    "campus": "Campus de Nantes",
    "datas": [
      {
        "date": "2018-10-14",
        "horaires": [
          {"nom": "", "deb": 800, "fin": 1400, "description": "", "couleur": ""},
          {"nom": "", "deb": 1630, "fin": 2000,  "description": "", "couleur": ""},
        ],
        "horaires_inter": [
          {"nom": "Accueil", "deb": 800, "fin": 1230,  "description": "bla bla si besoin", "couleur": "rgb(126, 255, 72)"},
          {"nom": "Permanence", "deb": 1230, "fin": 1400, "description": "bla bla si besoin", "couleur": "rgb(0, 255, 255)"},
          {"nom": "Accueil", "deb": 1630, "fin": 1830,  "description": "", "couleur": "rgb(126, 255, 72)"},
        ]
      },
    ]
  }
*/
module.exports = app;
