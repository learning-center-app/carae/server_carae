//Modules indispensables
const express = require('express');
const log = require('metalogger')();

//Modules spécifiques
const path = require('path');
const fs = require('fs');
const pug = require('pug');

//Prismic
const Cookies = require('cookies');
const Prismic = require('prismic-javascript');
const PrismicDOM = require('prismic-dom');
const PrismicConfig = require('./prismic-configuration');
const Onboarding = require('./onboarding');

const app = express.Router();

const MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG = {
  en : "en-gb",
  fr : "fr-fr",
}
/**
 * Traduit un code en tag en fonction des valeurs permises par Prismic
 * Exemple : "en" en code "en-gb"
 * @param {string} languageCode - ex: req.headers["accept-language"] == "en"
 * @returns {string} Code accepté par prismic - ex: "en-gb"
 */
function mapLanguageCodeToPrismic(languageCode) {
  // console.log({originalCode: languageCode});
  
  //Si le language n'est pas au format tag
  if(!languageCode.match("-")){
    //Renvoi tag harmonisé ou anglais si pas trouvé
    return MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG[languageCode] || MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG.en
  }
  //Possible qu'un languageTag soit envoyé (par les webview par exemple)
  else {
    //Si la valeur n'est pas dans celle permises par prismic
    if (Object.values(MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG).indexOf(languageCode) == -1){
      // Si la première partie du code correspond à un languageCode similaire
      let code = MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG[languageCode.match(/(.*?)\-/)]
      // On essaie de la rapprocher
      if (code)
        return code;
      // Sinon, en anglais par défaut
      else
        return MAP_LANGUAGE_CODE_TO_AVAILABLE_PRISMIC_TAG.fr
    }
    //Sinon c'est cool
    else
      return languageCode
  }
}
// Middleware to inject prismic context
app.get('/api/prismic/*', (req, res, next) => {
  res.locals.ctx = {
    endpoint: PrismicConfig.apiEndpoint,
    linkResolver: PrismicConfig.linkResolver,
  };
  // add PrismicDOM in locals to access them in templates.
  res.locals.PrismicDOM = PrismicDOM;
  // Rajout de la langue dans les locals pour moment.js
  res.locals.lang = req.headers['accept-language'] || "en";
  Prismic.api(PrismicConfig.apiEndpoint, {
    // accessToken: PrismicConfig.accessToken,
    req,
  }).then((api) => {
    req.prismic = { api };
    next();
  }).catch((error) => {
    log.error(error)
    next(error.message);
  });
});

//Page actualités
app.get('/api/prismic/actualites/:page?', function(req, res, next) {
  req.prismic.api.query(
    Prismic.Predicates.at('document.type', "actualite"),
    {
      orderings : '[my.actualite.date_publication desc]',
      pageSize : 10,
      page: req.params.page ? req.params.page : 1,
      lang: mapLanguageCodeToPrismic(req.headers['accept-language']),
    }
  )
  .then(function(_actualites) {
    log.debug(_actualites);
    let id_actualites = _actualites.results.map((item) => {
      return `/api/prismic/actualite/${item.uid}`
    })
    res.json(id_actualites);
  }, function noPrismicDocument(err) {
    log.error(err);
    next();
  });
});

app.get('/api/prismic/actualite/:id', function(req, res, next) {

    log.debug("Requete Prismic page : ",req.params.page);
    getPrismicPage('actualite', req.params.id, req, res, next, 'actualite');
});


//Page actualités
app.get('/api/prismic/descriptions', function(req, res, next) {
  req.prismic.api.query(
    Prismic.Predicates.at('document.type', "description"),
    {
      orderings : '[my.description.index]',
      pageSize : 10,
      page: req.params.page ? req.params.page : 1,
      lang: mapLanguageCodeToPrismic(req.headers['accept-language'])
    }
  )
  .then(function(_descriptions) {
    log.debug(_descriptions);
    let id_descriptions = _descriptions.results.map((item) => {
      return `/api/prismic/description/${item.uid}`
    })
    res.json(id_descriptions);
  }, function noPrismicDocument(err) {
    log.error(err);
    next();
  });
});

app.get('/api/prismic/description/:id', function(req, res, next) {

    log.debug("Requete Prismic page : ",req.params.page);
    getPrismicPage('description', req.params.id, req, res, next, 'description');
});


app.get('/api/prismic/cgu', async function (req, res, next) {

  log.debug("Requete CGU");
  let result = await getPrismicPageJSONAndHTML('page', 'cgu', req, res, next, 'cgu').catch((e)=>{res.json(e)})
  res.json({
    html: result.html,
    version: result.json.results[0].last_publication_date
  });
});

app.get('/api/prismic/cgu/web', async function (req, res, next) {

  log.debug("Requete CGU web");
  getPrismicPage('page', 'cgu', req, res, next, 'cgu_web')
});

//Service des templates jade, avec appel a prismic
app.get('/api/prismic/:page', function(req, res) {
  log.debug("Requete Prismic page : ",req.params.page);
  getPrismicPage('page', req.params.page, req, res);
});

function getPrismicPage(type, page, req, res, next, filename) {
  filename = filename || page;
  console.log(req.headers['accept-language']);
  
  req.prismic.api.getByUID(type || 'page', page, { lang: mapLanguageCodeToPrismic(req.headers['accept-language']) }).then(function(_document) {

    if(!_document){
      log.error("Document prismic non trouvé");
      return res.render(path.join(__dirname, 'views', "404"), {
        title: "Page introuvable"
      });
    }
    // log.debug(_document);

    res.locals.document = _document;
    var _path = path.join(__dirname, 'views', filename);

    fs.access(_path + ".pug", fs.constants.F_OK, function (err) {
      //S'il n'y a pas de template associé
      if (err){
        log.error(err)
        //On prend celui par défaut, qui affiche les slices et le header
        _path = path.join(__dirname, 'views', 'default');
      }
      var title = (_document.data.titre || _document.data.title)[0].text,
          default_meta = {
            seo_title: process.env.SEO_TITLE,
            seo_description: process.env.SEO_DESCRIPTION,
            seo_image: process.env.SEO_IMAGE
          };
      res.render(_path, {
        title: title,
        url: process.env.SERVER_URL + req.originalUrl,
        uid: _document.uid,
        meta: {
          seo_title : (_document.seo_title && _document.seo_title.length ? PrismicDOM.RichText.asText(_document.seo_title).trim() : (title ? (title + " - " + default_meta.seo_title) : default_meta.seo_title)),
          seo_description : (_document.seo_description && _document.seo_description.length ? PrismicDOM.RichText.asText(_document.seo_description).trim() : default_meta.seo_description),
          seo_image : (_document.seo_image ? _document.seo_image.url : (_document.image_banniere ? _document.image_banniere.url : default_meta.seo_image))
        }
      });
    })
  }, function noPrismicDocument(err) {
    res.render(path.join(__dirname, 'views', "404"), {
      title: "Page introuvable"
    });
  });
}

function getPrismicPageJSONAndHTML(type, page, req, res, next, filename) {
  return new Promise((resolve, reject) => {
    filename = filename || page;
    console.log(req.headers['accept-language']);
    req.prismic.api.query(
      Prismic.Predicates.at('my.page.uid', page), 
      { lang: mapLanguageCodeToPrismic(req.headers['accept-language']) }
    ).then(function(response) {
      //Response contient tout le retour JSON
      if(!response.results.length){
        log.error("Document prismic non trouvé");
        reject({
          html: pug.compileFile(path.join(__dirname, 'views', "404.pug"))({ title: "Page introuvable" }),
          json: response
        })
      }
      //Unwrap du document récupéré
      let _document = response.results[0];

      var _path = path.join(__dirname, 'views', filename) + ".pug";

      fs.access(_path, fs.constants.F_OK, function (err) {
        //S'il n'y a pas de template associé
        if (err){
          log.error(err)
          //On prend celui par défaut, qui affiche les slices et le header
          _path = path.join(__dirname, 'views', 'default.pug');
        }
        var title = (_document.data.titre || _document.data.title)[0].text,
        default_meta = {
          seo_title: process.env.SEO_TITLE,
          seo_description: process.env.SEO_DESCRIPTION,
          seo_image: process.env.SEO_IMAGE
        },
        locals = Object.assign(res.locals, {
          document: _document,
          title: title,
          url: process.env.SERVER_URL + req.originalUrl,
          uid: _document.uid,
          meta: {
            seo_title : (_document.seo_title && _document.seo_title.length ? PrismicDOM.RichText.asText(_document.seo_title).trim() : (title ? (title + " - " + default_meta.seo_title) : default_meta.seo_title)),
            seo_description : (_document.seo_description && _document.seo_description.length ? PrismicDOM.RichText.asText(_document.seo_description).trim() : default_meta.seo_description),
            seo_image : (_document.seo_image ? _document.seo_image.url : (_document.image_banniere ? _document.image_banniere.url : default_meta.seo_image))
          }
        })

        let _html = pug.compileFile(_path)(locals);

        resolve({
          html: _html,
          json: response
        })
      })
    }, function noPrismicDocument(err) {
      reject({
        html: pug.compileFile(path.join(__dirname, 'views', "404.pug"))({ title: "Page introuvable" }),
        json: {}
      })
    });
  })
}
module.exports = app;
